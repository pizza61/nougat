<p align="center"><img width=200px src="https://raw.githubusercontent.com/pizza61/nougat/master/nougat-maly.png" /></p>

![GitHub top language](https://img.shields.io/github/languages/top/pizza61/nougat.png?style=for-the-badge)
![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/pizza61/nougat.svg?style=for-the-badge)
![GitHub tag](https://img.shields.io/github/tag/pizza61/nougat.svg?style=for-the-badge)

## Nougat
Nougat to bot na Discorda. Posiada wiele fajnych funkcji, takich jak ekonomia, sadownictwo czy różne efekty obrazków

**Wersja 3.0 jeszcze nie jest w pełni gotowa, patrz TODO**

* [Strona bota, LISTA KOMEND](https://pizza61.github.io/nougat)
* [Invite bota](https://discordapp.com/api/oauth2/authorize?client_id=429587398511427584&permissions=469822598&scope=bot)

## Konfiguracja
Wymagania:
* mongodb
* node z npm
* [klucz do api youtube](https://developers.google.com/youtube/v3/getting-started)

- [ ] Po sklonowaniu repo należy wpisać komendę `npm install`, która zainstaluje wszystkie potrzebne zależności.
- [ ] Następnie należy utworzyć plik `settings.json` i uzupełnić go jak w pliku [settings.json.example](https://github.com/pizza61/nougat/blob/master/settings.json.example)
- [ ] Potem zbudować przy pomocy komendy `npm run build`
- [ ] Kiedy wszystko zostanie poprawnie uzupełnione, można wpisać `npm start` aby uruchomić bota.

## TODO
- [x] posegregowanie plików
- [ ] język angielski
- [ ] Zarządzanie członkami (banowanie, wyrzucanie)
- [x] Zarządzanie kanałem (czyszczenie wiadomości, wykrywanie zakazanych słów)
- [x] Autorole
