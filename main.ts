import { Nougat } from './src/main/main';

let Ng = new Nougat(require('../settings.json'));
Ng.init();
Ng.start();