import { Message, RichEmbed } from "discord.js";

let urban = "https://ocs-pl.oktawave.com/v1/AUTH_2887234e-384a-4873-8bc5-405211db13a2/splay/2018/03/milionerzy-milion-1180x664.jpg";

export default function milionerzy(message: Message) {
    const milionEmbed = new RichEmbed()
    .setAuthor('Nougat - Milionerzy!', urban)
    .setColor((Math.random() * 0xFFFFFF << 0).toString(16))
    .setImage(urban)
    .setTitle("Witaj w Nougatowych Milionerach!")
    .setDescription("Tylko u nas możesz wygrać okrągły milion BTCS (BTC serwerowych)")
}