import { GuildMember } from "discord.js";
import { autoroleService } from './autoroleService';
import { welcomeService } from './welcomeService';
import { Nougat } from "../main/main";

export function onJoin(member: GuildMember) {
    Nougat.Serwer.find({id: member.guild.id}, (err, dcs)=> {
        if(err) return;
        var kolczyk;
        dcs[0].members.forEach((klucz, index) => {
            console.log(klucz.id)
            if(klucz.id == member.guild.id) kolczyk = index;
        })
        if(!kolczyk) {
            dcs[0].members.push({
                id: member.id,
                nick: member.user.username,
                hajs: 0
            })
        }
    })
    autoroleService(member);
    welcomeService(member);
}